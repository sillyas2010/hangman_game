"use strict";


$(function(){
    var words = [
			'dog',
			'programming',
			'javascript',
			'jquery',
			'ball',
			'hangman',
			'cat',
	        'animals',
	        'student',
	        'amused',
	        'foregoing',
	        'war',
	        'roasted',
	        'tea',
	        'work',
	        'dust',
	        'sorry',
	        'room',
	        'resolution',
	        'step',
	        'time',
	        'battle',
	        'chicken',
	        'scream',
	        'null',
	        'cure',
	        'first',
	        'battle',
	        'theory'
                ];

    var game = new Game(words, '#letter-input', undefined, function(rules, ui){
        ui.UpdateWord(rules.Mask);
        var instance = new Notification(
			"Hangman Game", {
				body: "You're lucky, you're not in his shoes!",
                icon: "Hangman-Game-icon-fail.png"
			                }
		                               );
        var lost = new Audio('lost_final.mp3');
        lost.play();
    });
    $("ul").removeClass("default-ul");
    $("ul").addClass("empty-ul");

});
