"use strict";
function Game(words, input, onWin, onLose){
    GameBase.apply(this, [words, input]);
    this.Ui.DrawWord(this.Rules.Mask);
    if(typeof(onWin) === 'function')
        this.Win = function(){
            onWin(this.Rules, this.Ui);
            this.__proto__.ResetGame();
        };

    if(typeof(onLose) === 'function')
        this.Lose = function(){
            onLose(this.Rules, this.Ui);
            this.__proto__.ResetGame();
        };
}
Game.prototype = Object.create(GameBase.prototype);

Game.prototype.OnSuccess = function(){
    this.Ui.UpdateWord(this.Rules.Mask);
    if (this.Rules.IsWin) { this.Win(); }
};
Game.prototype.OnFailure = function(char){
    this.Ui.WrongLetter(char);
    this.Ui.Draw(this.Rules.Attempts - this.Rules.AttemptsLeft - 1);
    if (this.Rules.IsLose) { this.Lose(); }
};