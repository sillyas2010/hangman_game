"use strict";
function Rules(words){
    if(!words)
        words = ['world'];

    Object.defineProperty(this, 'Attempts', {value: 6});
    var fields = {
        attemptsLeft:0,
        answer:'',
        mask:''
    };

    var chooseWord = function() {
        return words[Math.floor(Math.random() * words.length)];
    };
    this.Reset = function(){
        fields.attemptsLeft = this.Attempts;
        fields.answer = chooseWord();
        fields.mask = new Array(fields.answer.length + 1).join( '_' );
    };

    
    /**
     * @param {string} char Character to check in word
     * @return {boolean} true if Character found; otherwise false
     */
    this.CheckCharacter = function(char){
        if(fields.attemptsLeft <= 0)
            alert('BROKEN!');
        char = char.toLowerCase();
        var isFound = false;
        for(var i = 0; i < fields.answer.length; i++)
            if(fields.answer[i] === char){
                isFound=true;
                fields.mask = fields.mask.replaceAt(i, char);
            }
        if(!isFound) {
            if(--fields.attemptsLeft == 0)
                fields.mask = fields.answer;
        }
        return isFound;
    };

    this.Reset();

    Object.defineProperty(this, 'Mask', {get: function () {
        return fields.mask;
    }});
    Object.defineProperty(this, 'AttemptsLeft', {get: function () {
        return fields.attemptsLeft;
    }});
    Object.defineProperty(this, 'IsWin', {get: function () {
        return fields.mask === fields.answer;
    }});
    Object.defineProperty(this, 'IsLose', {get: function () {
        return fields.attemptsLeft == 0;
    }});
}