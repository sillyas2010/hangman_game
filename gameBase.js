"use strict";

var GameBase = (function(){
    var rules;
    var ui = new HangmanUI();
    var gameBase = function(words, input){
        rules = new Rules(words);
        Object.defineProperty(this, 'Rules', {value: rules});
        var game = this;
        $(input).on('keyup', function(){
            var input = $(this);
            var char = input.val().toLowerCase();
            input.val('');
            if (char !== '') // BACKSPACE error_fix
            {
                var isFound = rules.CheckCharacter(char);
                if(isFound) {
                    game.OnSuccess(char);
                }
                else {
                    game.OnFailure(char);
                }
            }
        });
        Object.defineProperty(this, 'Ui', {value: ui});
    };

    gameBase.prototype.ResetGame = function() {
        rules.Reset();
        ui.Reset();
        ui.DrawWord(rules.Mask);
    };
    gameBase.prototype.Lose = function() {
        /*alert('Oh no, you lose!');*/
        var instance = new Notification(
			"Hangman Game", {
				body: "You're lucky, you're not in his shoes!",
                icon: "Hangman-Game-icon-fail.png"
			                }
		                               );
        var lost = new Audio('lost_final.mp3');
        lost.play();
        this.ResetGame();
    };
    
    gameBase.prototype.saveRecord = function() {
    
        if(localStorage){
            if(localStorage.getItem('name')===null){localStorage.setItem('name','default');}
            var name_p_word = (localStorage['name'] + " || " + rules.Mask);
            localStorage[name_p_word] =  ((rules.Attempts - rules.AttemptsLeft) + " mistakes");}
        else{alert("NO LOCAL STORAGE!")}
    };
    
    gameBase.prototype.Win = function() {
        /*alert('You win!');*/
        var instance = new Notification(
			"Hangman Game", {
				body: ("Nice, u answered right word! Congratulations! "),
                icon: "Hangman-Game-icon.png"
			                }
		                               );
        var won = new Audio('won_final.mp3');
        won.play();
        this.saveRecord();
        this.ResetGame();
        
    };
    
    gameBase.prototype.OnSuccess = function(){
        throw (undefined);
    };
    gameBase.prototype.OnFailure = function(){
        throw (undefined);
    };

    return gameBase;
})();