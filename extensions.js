"use strict";

/**
 * Replace character at specified position.
 *
 * @param {number} index Position.
 * @param {string} character Character to replace for.
 * @return {string} Changed string.
 */
String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
};


function findAllItems() {
    
  $("ul").removeClass("empty-ul");
  $("ul").addClass("default-ul");
    
  // Получаем элемент <ul> для списка элементов данных
  var itemList = document.getElementById("itemList");
  
  // Очищаем список
  itemList.innerHTML = "";

  // Перебираем все элементы данных в цикле
  for (var i=0; i<(localStorage.length); i++) {
      // Получаем ключ текущего элемента
    var key = localStorage.key(i);
	
	// Получаем сам элемент, хранящийся под этим ключом
    
    var item = localStorage[key];

    // Заполняем список
    var newItem = document.createElement("li");
    newItem.innerHTML = key + ": " + item;
    itemList.appendChild(newItem);
    itemList.appendChild(document.createElement("br"));
  }
};

function saveName() {
   
    localStorage.setItem('name', $("#nick-field").val()) ;
    $("#nick-field").val("")
    var name_notif = new Notification(
			"Hangman Game", {
				body: ("Your name was succesfully saved, " + localStorage.getItem('name') +"!"),
                icon: "Hangman-Game-icon.png"
			                }
		                               );
};



