"use strict";
function HangmanUI(){
    var classes = {
        shownLetter:'shown-letter'
    };
    Object.defineProperty(this, 'classes', {value: classes});
    var drawArea = $('.draw-area');

    var drawFuncs = {
        head : function () {
            drawArea.append
            (
                $('<div>').addClass('body-part head')
            );
        },
        torso : function() {
            drawArea.append
            (
                $('<div>').addClass('body-part armbox').append
                (
                    $('<div>').addClass('body-part torso')
                )
            );
            drawArea.append
            (
                $('<div>').addClass('body-part legbox').append
                (
                    $('<div>').addClass('body-part pelvis')
                )
            );
        },
        leftArm : function() {
            $('.armbox').prepend
            (
                $('<div>').addClass('body-part leftarm')
            );
        },
        rightArm : function() {
            $('.armbox').prepend
            (
                $('<div>').addClass('body-part rightarm')
            );
        },
        leftLeg : function() {
            $('.legbox').prepend
            (
                $('<div>').addClass('body-part leftleg')
            );
        },
        rightLeg : function() {
            $('.legbox').prepend
            (
                $('<div>').addClass('body-part rightleg')
            );
        }
    };
    var drawSequence =
        [
            drawFuncs.head,
            drawFuncs.torso,
            drawFuncs.leftArm,
            drawFuncs.rightArm,
            drawFuncs.leftLeg,
            drawFuncs.rightLeg
        ];

    Object.defineProperty(this, 'DrawSequence', {value: drawSequence});
}

HangmanUI.prototype.Reset = function(){
    $('.body-part').remove();
    $('.guessed-letter').remove();
    $('.shown-letter').remove();
};

/*
 * @param {string} word
 */
HangmanUI.prototype.DrawWord = function( word ){
    for (var i = 0; i < word.length; i++) {
        $('.word-display').append
        (
            $('<span>').addClass(this.classes.shownLetter).text(word[i])
        );
    }
};
/*
 * @param {string} word
 */
HangmanUI.prototype.UpdateWord = function( word ) {
    // Iterate over the list
    $('.' + this.classes.shownLetter).each(function(i)
    {
        if (word[i] === '*')
            $(this).html(' ');
        else
            $(this).html(word[i]);
    });
};

/*
 * @param {number} word
 */
HangmanUI.prototype.Draw = function(i){
    this.DrawSequence[i]();
};

/*
 * @param {string} letter
 */
HangmanUI.prototype.WrongLetter = function( letter ) {
    $('#wrong-letters').append
    (
        $('<span>').addClass('guessed-letter').html(letter)
    );
};